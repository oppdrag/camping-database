<?php
require_once "backend/database.php";

$file = fopen("./postnummer.csv", "r");

$postnummer = [];

while (!feof($file)) {
    $row = fgetcsv($file, null, "\t");

    if (!$row) {
        continue;
    }


    for ($i = 0; $i < count($row); $i++)
        $row[$i] = mb_convert_encoding($row[$i], "UTF-8", "ISO-8859-1");

    if (is_null($row[0]))
        continue;

    $postnummer[] = $row;
}

$db = database::getDatabaseConnection();

$query = "INSERT INTO postnummer (postnummer, poststed) VALUES ";

foreach ($postnummer as $item) {


    $nr = $item[0];
    $poststed = $item[1];

    $query .= "('$nr', '$poststed'),";
}

// remove trailing comma
$query = rtrim($query, ",");

$stmt = $db->query($query);
$stmt->execute();
