<?php
include_once "backend/getFromDatabase.php";
include_once "funksjoner/automagic.php";

if (!isset($_GET["table"])) {
    header("Location: index.php");
    die();
}

$uppercase = ucfirst(str_replace("-", " ", $_GET["table"]));
$page["title"] = "Table: $uppercase";

$navLinks = [
    ["link" => "index.php", "name" => "Home"],
    ["link" => "insert.php?table=$_GET[table]", "name" => "Insert"],
];

$page["body"] = function () {
    $primaryKey = "id";
    if ($_GET["table"] === "postnummer") $primaryKey = "postnr";
    $table = getFromDatabase::table($_GET["table"], $primaryKey);

    if (count($table) > 0) {
        foreach ($table as $key=>&$row) {
            $row += [
                "rediger" => "<a href='insert.php?table=$_GET[table]&row=$key'>rediger postnummer: $key</a>"
            ];
        }
    } else {
        $headersBefore = getFromDatabase::columns($_GET["table"]);
        $headers = [];
        $headers[] = [];
        foreach ($headersBefore as $i) $headers[0] += [
            $i => "",
        ];
        $table = $headers;
    }

    echo automagic::automagicTable($table);
};

include('template.php');
