<?php // filen før prøven it 1
class bilbasefunktjoner
{
    // Oppgave 1
    public static function erBilskiltetGjyldig($bilskilt, $drivstoff = "uviktig")
    {
        $prosesertBilskilt = str_split($bilskilt);

        // Sjekker om den ikke er gjyldig og returnerer false om den ikke er det
        if (count($prosesertBilskilt) !== 7) return false; // ikke gjyldig

        // split bokstavene og tall til to forskellie variabler
        $bokstaver = $prosesertBilskilt[0] . $prosesertBilskilt[1];
        $tall = (int)($prosesertBilskilt[2] . $prosesertBilskilt[3] . $prosesertBilskilt[4] . $prosesertBilskilt[5] . $prosesertBilskilt[6]);

        // om det ikke er 2 bokstaver i $bokstaver; returner false
        if (preg_match_all('/[a-zA-Z]/', $bokstaver) !== 2) return false;
        // om det ikke er 5 tall i $tall; returner false
        if (preg_match_all('/[1-9]/', $tall) !== 5) return false;

        if ($tall < 10000) {
            echo "Error: første sifferet i nummeret på skiltet er ikke 1 eller høyere";
            return false;
        }

        // om det er ellbil
        if (strtolower($prosesertBilskilt[0]) === "e") {
            if ($drivstoff !== "elektrisk" && $drivstoff !== "strøm" && $drivstoff !== "elektrisitet" &&
                $drivstoff !== "uviktig") {
                echo "Error: på nummerskiltet står det at bilen er elektrisk men infomasjon gitt sier at den er $drivstoff.";
                return false;
            }
        }

        return true;
    }

    // Oppgave 2
    public static function erDetDuplikatPerson($personnummer)
    {

        try {
            include "backend/database.php";

            $sql = "SELECT * FROM bileier WHERE personnummer = :personnummer";
            $stmt = $conn->prepare($sql);
            $stmt->bindParam(':personnummer', $personnummer);
            $stmt->execute();
            if ($stmt->rowCount() == 0) {
                return false;
            }
            //$bileier = $stmt->fetch(PDO::FETCH_ASSOC);
        } catch (PDOException $e) {
            echo "Kunne ikke koble til databasen: " . $e->getMessage();
            echo "derfor vet vi ikke om det er duplikat; returnerer at det ikke er det.";
            return false;
        }

        return true;
    }

    // Oppgave 3
    public static function fargeStatestikk()
    {
        // Jeg ble ikke ferdig med denne, men jeg var ganske nærme
        // du kan se på løsningen min, den mangler bare noe database tilkobling ting
        try {
            include "backend/database.php";

            $sql = "SELECT farge, COUNT(farge) as anntall FROM registrering group by farge;";
            $result = conn->query($sql);
            if ($result->rowCount() == 0) {
                echo "ingen registreringer";
                die();
            }
            foreach ($stmt->fetch(PDO::FETCH_ASSOC) as $key => $i) {
                $result[] = $i;
            }
            var_dump($result);
            $totalVotes = 0;
            foreach ($result as $i) {
                $totalVotes += $i["anntall"];
            }

                //$bileier = $stmt->fetch(PDO::FETCH_ASSOC);
        } catch (PDOException $e) {
            echo "Kunne ikke koble til databasen: " . $e->getMessage();
            die();
        }
        ?>
        <table>
            <tr>
                <th>farge</th>
                <th>anntall</th>
                <th>kakediagram</th>
            </tr>
            <?php foreach ($result as $key => $i) { ?>
                <tr>
                    <td><?php echo $i["farge"]; ?></td>
                    <td><?php echo $i["anntall"]; ?></td>
                    <td>
                        <img src="diagrammer/pieChart.php?value=<?php echo (int)$i["anntall"];
                        ?>&other=<?php echo $totalVotes - (int)$i["anntall"]; ?>" alt="">
                    </td>
                </tr>
            <?php } ?>
        </table>

    <?php }
}
