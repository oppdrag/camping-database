<?php
function getAllForms(): array {
    require_once "backend/getFromDatabase.php";

    function GetAllPostnummer (): array {
        return getFromDatabase::table("postnummer");
    }

    function GetAllValidPrisklasser (): array {
        return getFromDatabase::query("SELECT * FROM prisklasser WHERE (fradato <= NOW()) AND (tildato >= NOW())");
    }

    function GetAllKunder (): array {
        return getFromDatabase::table("kunde");
    }

    function GetAllLeieplasser (): array {
        return getFromDatabase::table("leieplass");
    }

    return [
        "elektrisitet" => [
            [
                "type" => "number",
                "name" => "pris_pr_kwh",
                "min" => 0,
                "required" => "1"
            ],
            [
                "type" => "date",
                "name" => "fradato",
                "required" => "1"
            ],
            [
                "type" => "date",
                "name" => "tildato",
                "required" => "1"
            ]
        ],

        "kunde" => [
            [
                "type" => "text",
                "name" => "fornavn",
                "maxlength" => 100,
                "required" => "1"
            ],
            [
                "type" => "text",
                "name" => "etternavn",
                "maxlength" => 100,
                "required" => "1"
            ],
            [
                "type" => "text",
                "name" => "adresse",
                "maxlength" => 100,
                "required" => "1"
            ],
            [
                "type" => "select",
                "name" => "postnummer",
                "required" => "1",
                "options" => array_map(function ($item) {
                    return $item["postnummer"];
                }, GetAllPostnummer())
            ],
            [
                "name" => "telefonnummer",
                "type" => "text",
                "required" => "1",
                "minlength" => 1,
                "maxlength" => 15
            ],

            [
                "type" => "email",
                "name" => "mail",
                "maxlength" => 255,
                "required" => "1"
            ],
            [
                "type" => "text",
                "name" => "kommentar",
                "maxlength" => 300,
                "required" => "1"
            ]
        ],

        "leieplass" => [
            [
                "type" => "number",
                "required" => "1",
                "min" => 0,
                "name" => "lengde"
            ],
            [
                "type" => "number",
                "required" => "1",
                "min" => 0,
                "name" => "bredde"
            ],
            [
                "type" => "select",
                "required" => "1",
                "name" => "type",
                "options" => [
                    "telt",
                    "campingvogn",
                    "bobil"
                ]
            ],
            [
                "type" => "checkbox",
                "value" => "1",
                "name" => "handikapvennlig"
            ],
            [
                "type" => "checkbox",
                "value" => "1",
                "name" => "elektrisitet"
            ],
            [
                "type" => "checkbox",
                "value" => "1",
                "name" => "vann"
            ],
            [
                "type" => "checkbox",
                "value" => "1",
                "name" => "husdyr"
            ],
            [
                "name" => "prisklasse_id",
                "type" => "select",
                "options" => array_map(function ($item) {
                    return [
                        "value" => $item["id"],
                        "display" => $item["prisklasse"] . " - " . $item["dognpris"] . "kr pr. døgn"
                    ];

                }, GetAllValidPrisklasser())
            ],
            [
                "name" => "kommentar",
                "type" => "text",
                "required" => "1"
            ],
        ],

        "leietid" => [
            [
                "name" => "kunde_id",
                "type" => "select",
                "options" => array_map(function ($value) {
                    return [
                        "value" => $value["id"],
                        "display" => $value["fornavn"] . " " . $value["etternavn"] . " - " . $value["mail"]
                    ];
                }, GetAllKunder())
            ],
            [
                "name" => "leieplass_id",
                "type" => "select",
                "options" => array_map(function ($leieplass) {

                    return [
                        "value" => $leieplass["id"],
                        "display" => $leieplass["kommentar"]
                    ];

                }, GetAllLeieplasser())
            ],
            [
                "name" => "bilnummer",
                "type" => "text",
                "required" => "1"
            ],
            [
                "name" => "stromforbruk_kwh",
                "type" => "number",
                "min" => "0",
                "required" => "1"
            ],
            [
                "name" => "vannforbruk_kubikkmeter",
                "type" => "number",
                "min" => "0",
                "required" => "1"
            ],
            [
                "name" => "kommentar",
                "type" => "text",
                "required" => "1"
            ],
            [
                "name" => "ankomst",
                "type" => "date",
                "required" => "1"
            ],
            [
                "name" => "avreise",
                "type" => "date",
                "required" => "1"
            ]

        ],

        "prisklasser" => [
                [
                    "type" => "text",
                    "name" => "prisklasse",
                    "maxlength" => 40,
                    "required" => "1",
                    "label" => "Navn"
                ],
                [
                    "type" => "number",
                    "name" => "dognpris",
                    "min" => 0,
                    "required" => "1"
                ],
                [
                    "type" => "date",
                    "name" => "fradato",
                    "required" => "1"
                ],
                [
                    "type" => "date",
                    "name" => "tildato",
                    "required" => "1"
                ]
            ],

    "vann" => [
            [
                "type" => "number",
                "name" => "pris_pr_kubikkmeter",
                "min" => 0,
                "required" => "1"
            ],
            [
                "type" => "date",
                "name" => "fradato",
                "required" => "1"
            ],
            [
                "type" => "date",
                "name" => "tildato",
                "required" => "1"
            ]
        ],
    ];
}