<?php
// laget av Gabriel
// copyright Gabriel Solem (c) 2024
// https://github.com/gekkojr or @gekkojr

$imgSize = 300;
$center = $imgSize / 2;

$totalVerdi = 0;
// finner totalverdien av alle elementer
foreach ($_GET as $amount)
{
    $totalVerdi = $totalVerdi + $amount;
}

$faktor = $totalVerdi/100;


// lager bilde tall indikerer størrelsen
$image = imagecreatetruecolor($imgSize, $imgSize);

// fyller det med transperent piksler
imagesavealpha($image, true);
$trans_color = imagecolorallocatealpha($image, 0,0,0,127);
imagefill($image, 0, 0, $trans_color);

// definerer farger
$red = imagecolorallocate($image, 255, 0, 0);
$green = imagecolorallocate($image, 0, 255, 0);

$colors = [
    imagecolorallocate($image, 0, 0, 255),
    imagecolorallocate($image, 199, 199, 210),
    imagecolorallocate($image, 214, 220, 22),
    imagecolorallocate($image, 2, 22, 39),
    imagecolorallocate($image, 20, 200, 200),
    imagecolorallocate($image, 233, 233, 0),
];

//nødvendige variabler
$index = 1;
$totalLength = sizeof($_GET);
$lastEnd = 0;

//lager figuren (burde ha en funksjon for tilfeldige fargr, siden resten er fullt dynaminsk)
foreach ($_GET as $element) {
    // regner ut størrelsen av hvert elementet
    $size = ($element / $faktor) * 3.6;

    // begynner på null første gang
    if($index == 1) {
        imagefilledarc($image, $center, $center, $imgSize, $imgSize, 0, $size, $red, IMG_ARC_PIE);
    } // om det er siste farge
    elseif ($index == $totalLength) {
        imagefilledarc($image, $center, $center, $imgSize, $imgSize, $lastEnd, 360, $green, IMG_ARC_PIE, );
    } // farger som ikke er først eller sist
    else {
        // regner ut når den skal stoppe (den starter hvor den forgje sluttet
        $stop = $lastEnd + $size;
        imagefilledarc($image, $center, $center, $imgSize, $imgSize, $lastEnd, $stop, $colors[$index-1], IMG_ARC_PIE );
    }

    // saves where new sector should begin
    $lastEnd = $lastEnd + $size;
    $index++;
}


header('Content-type: image/png');
imagepng($image);
imagedestroy($image);