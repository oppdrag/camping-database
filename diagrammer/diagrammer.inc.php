<?php

class Diagrammer
{
    public static function soylediagram($data, $divider = 1, $vertical = true)
    {
        if ($vertical) {
            $dir = "width: ";
        } else {
            $dir = "height: ";
        } ?>
        <ul class="<?php if ($vertical) {
            echo "soylediagramv";
        } else {
            echo "soylediagramh";
        }
        ?>">
            <?php foreach ($data as $key => $i) {
                //if ($key === 0) continue;
                ?>
                <li>
                    <span class="soyNavn"><?php echo $i["name"]; ?></span>
                    <br>
                    <div class="soyle">
                        <div class="soy1" style="<?php echo $dir . (string)($i["value"] / $divider) . "px"; ?>">
                            <span><?php echo (string)$i["value"]; ?></span>
                        </div>
                    </div>
                </li>
            <?php } ?>
        </ul>
    <?php }

}