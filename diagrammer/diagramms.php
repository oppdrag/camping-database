<?php
global $data;
include "data.php";
include "diagrammer/diagrammer.inc.php";

$page["title"] = "Data";

$page["body"] = function () { ?>
    <?php
    global $data;
    $totalVotes = 0;
    foreach ($data as $key => $i) {
        $totalVotes += (int)$i["value"];
    }
    ?>
    <table>
        <tr>
            <th>bilde</th>
            <th>stemmer</th>
            <th>kakediagram</th>
        </tr>
        <?php foreach ($data as $key => $i) { ?>
            <tr>
                <td><?php echo $i["name"]; ?></td>
                <td><?php echo $i["value"]; ?></td>
                <td>
                    <img src="diagrammer/pieChart.php?value=<?php echo (int)$i["value"];
                    ?>&other=<?php echo $totalVotes - (int)$i["value"]; ?>" alt="">
                </td>
            </tr>
        <?php } ?>
    </table>

    <?php
    Diagrammer::soylediagram($data, 0.01, true);
    ?>

<?php };

include('template.php');