<?php

class submitFunctions
{
    public static function insertIntoTable(string $tableName, array $valuesToInsert): int
    {
        // Array valuesToInsert uses the format:
        //      [
        //          "nameOfColumn" => "Value",
        //          "nameOfAnotherColumn" => "anotherValue",
        //      ]
        // this is inserted into the table $tableName

        if ($tableName === "leietid") {
            include_once "backend/database.php";

            function GetElektrisitetForPeriod(string $fradato): ?array {
                $db = database::getDatabaseConnection();
                $stmt = $db->prepare("SELECT * FROM elektrisitet WHERE fradato <= :fradato AND tildato >= :fradato LIMIT 1");
                $stmt->bindParam(":fradato", $fradato);
                $stmt->execute();
                if ($stmt->rowCount() === 0)
                    return null;

                return $stmt->fetch();
            }

            function GetLeieplassById(int $id): ?array {
                $db = database::getDatabaseConnection();
                $stmt = $db->prepare("SELECT * FROM leieplass JOIN prisklasser p on p.id = leieplass.prisklasse_id WHERE leieplass.id = :id");
                $stmt->bindParam(":id", $id);
                $stmt->execute();
                if ($stmt->rowCount() === 0)
                    return null;
                return $stmt->fetch();
            }

            function GetVannForPeriod(string $fradato): ?array {
                $db = database::getDatabaseConnection();
                $stmt = $db->prepare("SELECT * FROM vann WHERE fradato <= :fradato AND tildato >= :fradato LIMIT 1");
                $stmt->bindParam(":fradato", $fradato);
                $stmt->execute();
                if ($stmt->rowCount() === 0)
                    return null;

                return $stmt->fetch();
            }


            $kunde_id                   = $valuesToInsert["kunde_id"];
            $leieplass_id               = $valuesToInsert["leieplass_id"];
            $bilnummer                  = $valuesToInsert["bilnummer"];
            $stromforbruk_kwh           = $valuesToInsert["stromforbruk_kwh"];
            $vannforbruk_kubikkmeter    = $valuesToInsert["vannforbruk_kubikkmeter"];
            $kommentar                  = $valuesToInsert["kommentar"];
            $ankomst                    = $valuesToInsert["ankomst"];
            $avreise                    = $valuesToInsert["avreise"];

            $leieplass = GetLeieplassById($leieplass_id);
            $currentElektrisitet = GetElektrisitetForPeriod($ankomst);
            if (is_null($currentElektrisitet))
                throw new Exception("No electricity price is defined for the arrival day!");

            if ($leieplass["elektrisitet"])
                $elektrisitetPris = $currentElektrisitet["pris_pr_kwh"];
            else
                $elektrisitetPris = 0;


            $valuesToInsert["strompris_id"] = $currentElektrisitet["id"];
            $valuesToInsert["strompris_total"] = $stromforbruk_kwh * $elektrisitetPris;

            $currentVann = GetVannForPeriod($ankomst);
            if (is_null($currentVann))
                throw new Exception("No water price is defined for the arrival day!");

            if ($leieplass["vann"])
                $vannPris = $currentVann["pris_pr_kubikkmeter"];
            else
                $vannPris = 0;

            $valuesToInsert["vannpris_id"] = $currentVann["id"];
            $valuesToInsert["vannpris_total"] = $vannforbruk_kubikkmeter * $vannPris;


            $valuesToInsert["dogn_sum"] = $leieplass["dognpris"] * ((strtotime($avreise) - strtotime($ankomst)) / (60 * 60 * 24));

            $valuesToInsert["totalpris"] = $valuesToInsert["vannpris_total"] + $valuesToInsert["strompris_total"] +  $valuesToInsert["dogn_sum"];

        }

        function generateInsertSqlQuery(string $tableName, array $valuesToInsert): string
        {
            // example of what this function returns:
            //     string "INSERT INTO tickets ( title, description, category_id, author_id ) VALUES ( :title, :description, :category_id, :author_id ) ;"

            $sqlStart = "INSERT INTO $tableName ";
            $sqlStart .= "(";
            $sqlEnd = "VALUES ";
            $sqlEnd .= "(";
            foreach ($valuesToInsert as $column => $_) {
                $sqlStart .= " $column, ";
                $sqlEnd .= " :$column, ";
            }
            $sqlStart .= ") ";
            $sqlEnd .= ") ";

            // remove the last comma
            $sqlStartArray = str_split($sqlStart);
            $sqlEndArray = str_split($sqlEnd);
            array_splice($sqlStartArray, -4, 1);
            array_splice($sqlEndArray, -4, 1);
            $sqlStart = implode("", $sqlStartArray);
            $sqlEnd = implode("", $sqlEndArray);
            $sqlStartArray = null;
            $sqlEndArray = null;

            $sql = $sqlStart . $sqlEnd;

            $sql .= ";";
            return $sql;
        }

        include "backend/checkPost.php";
        try {
            include_once "backend/database.php";
            $conn = database::getDatabaseConnection();

            $sql = generateInsertSqlQuery($tableName, $valuesToInsert);

            $stmt = $conn->prepare($sql);

            foreach ($valuesToInsert as $column => $value) {
                $stmt->bindParam(":$column", $valuesToInsert[$column]);
            }

            $stmt->execute();
            return $conn->lastInsertId();
        } catch (PDOException $e) {
            echo "Error: " . $e->getMessage();
            return 0;
        }
    }

    public static function updateTable(string $tableName, array $primaryKey, array $valuesToChange): void
    {
        // Array $primaryKey should just contain [ "nameOfPrimaryKeyColumn" => "PrimaryKeyValue" ]
        // Array valuesToChange uses the same format:
        //      [
        //          "nameOfColumn" => "newValue",
        //          "nameOfAnotherColumn" => "anotherNewValue",
        //      ]

        function generateUpdateSqlQuery(string $tableName, array $primaryKey, array $valuesToChange): string
        {
            // example of what this function returns:
            //     string "UPDATE tickets set status_id = :statusId, category_id = :categoryId WHERE id = :ticketId ;"

            $sql = "UPDATE $tableName SET ";
            foreach ($valuesToChange as $column => $_) {
                $sql .= " $column = :$column, ";
            }
            // remove the last comma
            $sqlArray = str_split($sql);
            array_splice($sqlArray, -2, 1);
            $sql = implode("", $sqlArray);
            $sqlArray = null;

            foreach ($primaryKey as $primaryKeyColumn => $_) {
                $sql .= " WHERE $primaryKeyColumn = :$primaryKeyColumn ";
                break;
            }

            $sql .= ";";
            return $sql;
        }

        if (count($primaryKey) !== 1) {
            echo '$primaryKey array must only have one value formatted like [ "nameOfPrimaryKeyColumn" => "PrimaryKeyValue" ]';
        }

        include_once "backend/checkPost.php";
        try {
            include_once "backend/database.php";
            $conn = database::getDatabaseConnection();

            $sql = generateUpdateSqlQuery($tableName, $primaryKey, $valuesToChange);

            $stmt = $conn->prepare($sql);

            foreach ($primaryKey as $primaryKeyColumn => $value) {
                $stmt->bindParam(":$primaryKeyColumn", $primaryKey[$primaryKeyColumn]);
            }
            foreach ($valuesToChange as $column => $value) {
                $stmt->bindParam(":$column", $valuesToChange[$column]);
            }

            $stmt->execute();
            echo "Successfully updated table: $tableName";
        } catch (PDOException $e) {
            echo "Error: " . $e->getMessage();
        }
        $conn = null;
    }
}
